#!/usr/bin/python3

from math import log

def humansize(n, binary=False):

    lock = 0

    if isinstance(n, (float, int)):
        if n > 0 and n < 1208925819614629174706176:
            lock += 1

    if isinstance(binary, bool):
        lock += 1

    if lock == 2:
        b = ((1000, ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")), (1024, ("B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB")))[binary]
        r = int(log(n, b[0]))
        return f"{(n / (b[0] ** r)):.2f} {b[1][r]}"